<?php
function tentukan_nilai($number)
{
    if ($number <= 100 && $number >= 85) {
        return "nilai " . $number . " itu Sangat Baik <br>";
    } elseif ($number <= 85 && $number >= 70) {
        return "nilai " . $number . " itu Baik <br>";
    } elseif ($number <= 70 && $number >= 60) {
        return "nilai " . $number . " itu Cukup <br>";
    } else {
        return "nilai " . $number . " itu Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
