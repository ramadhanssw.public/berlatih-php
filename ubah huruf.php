<?php
function ubah_huruf($string)
{
    $alfabhet = "abcdefghijklmnopqrstuvwxyz";
    $keluaran = " menjadi ";
    for ($i = 0; $i < strlen($string); $i++) {
        $index = strrpos($alfabhet, $string[$i]);
        $keluaran .= substr($alfabhet, $index + 1, 1);
    }
    return $string . $keluaran . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
